# README #

### What is this repository for? ###

* This android application was created for use by students attending Cal State San Bernardino to sell their
* textbooks with other students. 
* Version: 1.0

### Flow of Application ###

* Users create account using their school email, name and password. All passwords are encrypted and stored in database.
* Upon login a user may search for a textbook or post their own textbook by filling out the 'post item' form. 
* Users contact each other through gmail since the email of the seller is provided. 
* User can log out. 

### Database Information ###

* This application uses sqlite database.
* It includes two tables; Users and Listings
* All information is stored on local database. 

### Who do I talk to? ###

* Repo Owner: Anissa Camacho