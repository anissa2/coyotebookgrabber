package com.example.ale.logincbg;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Dell on 3/1/2018.
 */

public class CustomAdapter extends SimpleCursorAdapter {

    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;
    Button btnView;
    private String id;


    public CustomAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.layout = layout;
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.cr = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        super.bindView(view, context, cursor);

        TextView textViewTitle = view.findViewById(R.id.txtTitle);
        String title = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_TITLE));
        textViewTitle.setText(title);

        TextView textViewAuthor = view.findViewById(R.id.txtAuthor);
        String author = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_AUTHOR));
        textViewAuthor.setText(author);


        TextView textViewPrice = view.findViewById(R.id.txtPrice);
        String price = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_PRICE));
        textViewPrice.setText(price);
        //Log.d("custom adapter", price);

        final String selectedItem = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.ID));
        Log.d("Custom Adapter", selectedItem);

        btnView = view.findViewById(R.id.viewBtn);

        btnView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent(context, ViewListingActivity.class);
                i.putExtra("listId", selectedItem);
                v.getContext().startActivity(i);

                Toast.makeText(context, "view clicked", Toast.LENGTH_SHORT).show();
                Log.d("Custom Adapter/Selected Item", selectedItem);
            }
        });

    }
}


