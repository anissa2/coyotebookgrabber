package com.example.ale.logincbg;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SignOutActivity extends AppCompatActivity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);

        UserDBAdapter db = new UserDBAdapter(this);
        db.open();
        db.unsetLoggedUser();
        db.close();
    }

}