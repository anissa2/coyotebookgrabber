package com.example.ale.logincbg;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    //Shared Preferences will allow user variables to be used through out the application
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    // UI references.
    Button Login;

    private EditText mPasswordView, mUserName;
    private View mProgressView;
    private View mLoginFormView;
    CheckBox mCheckBox;
    String decrypPass, savedPass, savedUser, profileEmail, profileFname, profileLname;

    //for context database usage
    Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mUserName = findViewById(R.id.txtUserName);
        mPasswordView = findViewById(R.id.txtPassword);
        mCheckBox = findViewById(R.id.CHK_remember_me);

        Login = findViewById(R.id.btnLogin);

        mPreferences = getSharedPreferences("student_info.db", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();

        mEditor.putString("key", UserDBAdapter.USER_USERNAME);
        mEditor.apply();
        checkSharedPreferences();

        Login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String UserName = mUserName.getText().toString();//get input username from user
                String Pwd = mPasswordView.getText().toString(); //get input pass from user

                UserDBAdapter db = new UserDBAdapter(ctx);
                db.open();
                //get login info
                Cursor CR = db.getLoginInfo(UserName);


                String helper = DatabaseUtils.dumpCursorToString(CR);
                Log.d("Login helper", helper);

                savedUser = CR.getString(CR.getColumnIndex("username"));
                Log.d("The saved user 1", savedUser);
                savedPass = CR.getString(CR.getColumnIndex("password"));
                Log.d("The saved pass 1", savedPass);

                //decrypt password from database
                try {
                    decrypPass = AESCrypt.decrypt(savedPass);
                    Log.d("Decrypted Pass", decrypPass);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                boolean loginStatus = false;

                do {
                    if (UserName.equals(savedUser) && (Pwd.equals(decrypPass))) {
                        loginStatus = true;
                    }
                } while (CR.moveToNext());

                //log user in and check if they want their credentials remembered or not
                if (loginStatus && mCheckBox.isChecked()) {

                    //set checkbox when application starts
                    mEditor.putString(getString(R.string.checkbox), "True");
                    mEditor.apply();

                    //save the name
                    String name = mUserName.getText().toString();
                    mEditor.putString(getString(R.string.username), name);
                    mEditor.commit();

                    //save the password
                    String password = mPasswordView.getText().toString();
                    mEditor.putString(getString(R.string.password), password);
                    mEditor.commit();

                    Intent MainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(MainIntent);
                    Toast.makeText(LoginActivity.this, "You've signed in successfully.", Toast.LENGTH_LONG).show();

                    //set user as logged
                    db.setLoggedUser(UserName);

                    //if user has correct credentials get info to display in profile navigation
                    profileEmail = db.getUserEmail();
                    profileFname = db.getFname();
                    profileLname = db.getLname();
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("userEmail", profileEmail);
                    extras.putString("userFname", profileFname);
                    extras.putString("userLname", profileLname);
                    i.putExtras(extras);
                    startActivity(i);

                    Log.d("Logged User Email", profileEmail);

                } else if (loginStatus && !mCheckBox.isChecked()) {

                    //set checkbox to false
                    mEditor.putString(getString(R.string.checkbox), "False");
                    mEditor.commit();

                    //set to empty string
                    mEditor.putString(getString(R.string.username), "");
                    mEditor.commit();

                    //set to empty string
                    mEditor.putString(getString(R.string.password), "");
                    mEditor.commit();

                    Intent MainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(MainIntent);
                    Toast.makeText(LoginActivity.this, "You've signed in successfully.", Toast.LENGTH_LONG).show();

                    //set logged user
                    db.setLoggedUser(UserName);

                    //if user has correct credentials get info to display in profile navigation
                    profileEmail = db.getUserEmail();
                    profileFname = db.getFname();
                    profileLname = db.getLname();
                    Log.d("Login Helper", profileFname);
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("userEmail", profileEmail);
                    extras.putString("userFname", profileFname);
                    extras.putString("userLname", profileLname);
                    i.putExtras(extras);
                    startActivity(i);


                } else {
                    Toast.makeText(LoginActivity.this, "Sorry, username or password is incorrect.", Toast.LENGTH_LONG).show();
                }
                db.close();



                }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void checkSharedPreferences() {
        String checkBox = mPreferences.getString(getString(R.string.checkbox), "False");
        String name = mPreferences.getString(getString(R.string.username), "");
        String password = mPreferences.getString(getString(R.string.password), "");

        //set values to textfields
        mUserName.setText(name);
        mPasswordView.setText(password);

        //handle checkbox. If checked(true)set checkbox if unchecked(false) unset
        if (checkBox.equals("True")) {
            mCheckBox.setChecked(true);
        } else {
            mCheckBox.setChecked(false);
        }
    }

}

