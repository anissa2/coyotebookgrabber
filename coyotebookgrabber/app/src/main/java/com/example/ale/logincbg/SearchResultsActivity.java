package com.example.ale.logincbg;

import android.app.ListActivity;
import android.content.Context;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.content.Intent;
import android.app.SearchManager;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Dell on 12/4/2017.
 */

public class SearchResultsActivity extends ListActivity {

    ListingDBAdapter db = new ListingDBAdapter(this);
    Cursor cursor;
    String searchQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_activty);
        displayList();
    }

    public void displayList() {
        db.open();

        Intent intent = getIntent();

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            searchQuery = intent.getStringExtra(SearchManager.QUERY);
            Log.d("Search Query", searchQuery);

            cursor = db.searchListing(searchQuery);
            String results = DatabaseUtils.dumpCursorToString(cursor);
            Log.d("Search Results Activity 1", results);
        }

        String fromFieldNames[] = new String[]{db.POSTS_TEXT_TITLE, db.POSTS_TEXT_AUTHOR, db.POSTS_TEXT_PRICE};
        int[] toViewListings = new int[]{R.id.txtTitle, R.id.txtAuthor, R.id.txtPrice};

        final CustomAdapter sc = new CustomAdapter(getBaseContext(), R.layout.list_layout, cursor, fromFieldNames, toViewListings);

        ListView list = findViewById(android.R.id.list);
        list.setAdapter(sc);

        list.setItemsCanFocus(true);
        /*
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                db.close();
                String selectedItem = cursor.getString(cursor.getColumnIndex(db.ID));
                Log.d("ID", selectedItem);
                Intent intent = new Intent(SearchResultsActivity.this, DisplayListingActivity.class);
                intent.putExtra("listId", selectedItem);
                startActivity(intent);

                Toast.makeText(SearchResultsActivity.this, "view clicked", Toast.LENGTH_SHORT).show();
                //btnView.setTag(1);

            }
        });
        */

    }

}
