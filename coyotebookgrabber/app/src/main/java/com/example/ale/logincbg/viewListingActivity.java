package com.example.ale.logincbg;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Dell on 3/1/2018.
 */

public class ViewListingActivity extends AppCompatActivity{
    String id;
    ListingDBAdapter db = new ListingDBAdapter(this);
    TextView title, author, price, isbn, condition, description, subject, courseNum, profName, email;
    Cursor cursor;
    ImageView image;
    //to view image
    Bitmap viewImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_listing);

        db.open();

        Bundle extra = getIntent().getExtras();
        id = extra.getString("listId");
        int idList = Integer.parseInt(id);

        cursor = db.getAll(idList);

        title = findViewById(R.id.titleTxt);
        author = findViewById(R.id.authorTxt);
        price = findViewById(R.id.priceTxt);
        isbn = findViewById(R.id.isbnTxt);
        condition = findViewById(R.id.conditionTxt);
        description = findViewById(R.id.descriptionTxt);
        subject = findViewById(R.id.subTxt);
        courseNum = findViewById(R.id.courseNumTxt);
        profName = findViewById(R.id.profNameTxt);
        image = findViewById(R.id.postImageView);
        email = findViewById(R.id.emailTxt);

        byte [] originalImage = cursor.getBlob(9);
        viewImage = convertImage(originalImage);

        title.setText(cursor.getString(cursor.getColumnIndex("title")));
        author.setText(cursor.getString(cursor.getColumnIndex("author")));
        price.setText(cursor.getString(cursor.getColumnIndex("price")));
        isbn.setText(cursor.getString(cursor.getColumnIndex("isbn")));
        condition.setText(cursor.getString(cursor.getColumnIndex("condition")));
        description.setText(cursor.getString(cursor.getColumnIndex("description")));
        subject.setText(cursor.getString(cursor.getColumnIndex("subject")));
        courseNum.setText(cursor.getString(cursor.getColumnIndex("course_number")));
        profName.setText(cursor.getString(cursor.getColumnIndex("professor")));
        email.setText(cursor.getString(cursor.getColumnIndex("coyote_email")));

        image.setImageBitmap(viewImage);

        db.close();
    }

    // convert from byte array to bitmap
    public static Bitmap convertImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}
