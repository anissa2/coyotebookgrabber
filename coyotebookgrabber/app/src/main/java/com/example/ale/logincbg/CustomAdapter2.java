package com.example.ale.logincbg;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Created by Dell on 3/1/2018.
 */

public class CustomAdapter2 extends SimpleCursorAdapter {

    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;
    Button btnEdit;
    private String id;


    public CustomAdapter2(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.layout = layout;
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.cr = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        super.bindView(view, context, cursor);

        TextView viewListingTitle = view.findViewById(R.id.listTitleTxt);
        String titleView = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_TITLE));
        viewListingTitle.setText(titleView);

        TextView viewListingAuthor = view.findViewById(R.id.listAuthorTxt);
        String authorView = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_AUTHOR));
        viewListingAuthor.setText(authorView);


        TextView viewListingPrice = view.findViewById(R.id.listPriceTxt);
        String priceView = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.POSTS_TEXT_PRICE));
        viewListingPrice.setText(priceView);
        //Log.d("custom adapter", price);

        final String selectedItem = cursor.getString(cursor.getColumnIndex(ListingDBAdapter.ID));
        Log.d("Custom Adapter2", selectedItem);

        btnEdit = view.findViewById(R.id.editBtn);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, EditListingsActivity.class);
                intent.putExtra("displayId", selectedItem);
                v.getContext().startActivity(intent);
                Toast.makeText(context, "edit clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
