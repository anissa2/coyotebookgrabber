package com.example.ale.logincbg;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.SQLException;
import android.util.Log;

/**
 * Created by Dell on 1/3/2018.
 */

public class DBAdapter {

    public static final String DATABASE_NAME = "students.db"; //$NON-NLS-1$

    public static final int DATABASE_VERSION = 1;

    //creates query: create table
    private static final String CREATE_USER_TABLE = "CREATE TABLE " + UserDBAdapter.DATABASE_TABLE_USERS
            + "(" + UserDBAdapter.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UserDBAdapter.USER_FNAME + " TEXT NOT NULL, "
            + UserDBAdapter.USER_LNAME + " TEXT NOT NULL, "
            + UserDBAdapter.USER_USERNAME + " TEXT NOT NULL, "
            + UserDBAdapter.USER_COYOTE_EMAIL + " TEXT NOT NULL, "
            + UserDBAdapter.USER_PASS + " TEXT NOT NULL, "
            + UserDBAdapter.USER_IS_CURRENT_USER + " STRING NOT NULL);";

    //creates query: create listings
    private static final String CREATE_POSTS_TABLE = "CREATE TABLE " + ListingDBAdapter.DATABASE_TABLE_POSTS
            + "(" + ListingDBAdapter.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ListingDBAdapter.POSTS_TEXT_TITLE + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_TEXT_AUTHOR + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_TEXT_ISBN + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_PROFESSOR_NAME + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_COURSE_NUMBER + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_TEXT_PRICE + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_TEXT_DESCRIPTION + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_TEXT_CONDITION + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_COURSE_SUBJECT + " TEXT NOT NULL, "
            + ListingDBAdapter.POSTS_IMAGE + " BLOB, "
            + ListingDBAdapter.POSTS_USER_EMAIL + " TEXT NOT NULL);";


    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    /**
     * Constructor
     * @param ctx
     */
    public DBAdapter(Context ctx)
    {
        this.context = ctx;
        this.DBHelper = new DatabaseHelper(this.context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(CREATE_USER_TABLE);
            Log.d("Database Helper", "User Table Created");
            db.execSQL(CREATE_POSTS_TABLE);
            Log.d("Database Helper", "Posts Table Created");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion)
        {
            db.execSQL("DROP TABLE IF EXISTS " + UserDBAdapter.DATABASE_TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " + ListingDBAdapter.DATABASE_TABLE_POSTS);
            onCreate(db);
        }
    }

    /**
     * open the db
     * @return this
     * @throws SQLException
     * return type: DBAdapter
     */
    public DBAdapter open() throws SQLException
    {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    /**
     * close the db
     * return type: void
     */
    public void close()
    {
        this.DBHelper.close();
        Log.d("Database Helper", "Database closed");
    }

}
