package com.example.ale.logincbg;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by Dell on 3/12/2018.
 */

public class EditListingsActivity extends Activity {

    ListingDBAdapter db = new ListingDBAdapter(this);
    Cursor cur;
    Spinner spinnerCondition, spinnerSubject;
    Button btnUpdate;
    EditText EDIT_TITLE,  EDIT_AUTHOR,  EDIT_ISBN,  EDIT_PROFNAME,  EDIT_COURSENUM,  EDIT_PRICE,  EDIT_DESCRIPTION;
    String listingID, titleEdit, authorEdit, isbnEdit, profNameEdit, courseNumEdit, priceEdit, descriptionEdit, conditionEdit, subjectEdit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_listing_activity);

        //for spinner
        String[] book_condition = new String[]{"Good",
                "Bad",
                "Acceptable"};

        String[] course_subject = new String[]{
                "ACCT - Accounting",
                "ADMN - Administration",
                "AMST - American Studies",
                "ANTH - Anthropology",
                "ARAB - Arabic",
                "ART - Art",
                "AS - Aerospace Studies",
                "ASIA - Asian Studies",
                "ASL - American Sign Language",
                "ASTR - Astronomy",
                "BIOL - Biology",
                "CAL - Arts and Letters",
                "CD - Child Development",
                "CHEM - Chemistry",
                "CHIN - Chinese",
                "CJUS - Criminal Justice",
                "CM - Course Math",
                "COMM- Communication Studies",
                "CSE - Computer Science and Engineering",
                "CSU - Concurrent Enrollment",
                "DAN - Dance",
                "EADM - Educational Administration",
                "EADV - Advanced Education",
                "ECLG - Educational Counseling",
                "ECON - Economics",
                "ECTS - Career and Technical Studies",
                "EDCA - Correctional and Alternative",
                "ENG - English",
                "EDCI - Curriculum and Instruction",
                "EDSC - Science Education",
                "EDSP - School Psychology",
                "EDUC - Education",
                "EELB - Elementary/Bilingual Education",
                "EENV - Environmental Education",
                "EESL - Educ Eng Speakers other Lang",
                "ENG - English",
                "ENTR - Entrepreneurship",
                "ERDG - Reading Education",
                "EREH - Rehabilitiation Counseling",
                "ES - Ethnic Studies",
                "ESBM - School Business Management",
                "ESEC - Secondary Education",
                "ESPE - Special Education",
                "ESTM - Sci, Tech, Engin, Math Edu",
                "ETEC - Instructional Technology",
                "EVOC - Vocational Education",
                "FIN - Finance",
                "FLAN - Foreign Language",
                "FREN - French",
                "GEOG - Geography",
                "GEOL - Geology",
                "GER - German",
                "GSS - Gender and Sexuality Studies",
                "HD - Human Development",
                "HIST - History",
                "HON - Honors",
                "HRM - Human Resource Management",
                "HSCI - Health Science",
                "HUM - Humanities",
                "IP - International Program",
                "IS- Interdisciplinary Studies",
                "ISA - Interdisciplinary Studies Abro",
                "IST- Information Systems and Tech",
                "JAPN - Japanese",
                "KINE - Kinesiology",
                "KOR - Korean",
                "MAND - Mandarin",
                "MATH - Mathematics",
                "MGMT - Management",
                "MILS - Military Science",
                "MKTG - Marketing",
                "MUS - Music",
                "NSCI - Natural Sciences",
                "NSE - National Student Exchange",
                "NURS - Nursing",
                "OLLI - Lifelong Learning-OSHER",
                "PA - Public Administration",
                "PERS - Persian",
                "PHIL - Philosophy",
                "PHYS - Physics",
                "PLST - Paralegal Studies",
                "PSCI - Political Science",
                "PSYC - Psychology",
                "SCM - Supply Chain Management",
                "SOC - Sociology",
                "SPAN - Spanish",
                "SSCI - Social Sciences",
                "SW - Social Work",
                "TA - Theatre Arts",
                "USTD - University Studies",
        };

        ArrayAdapter<String> adapter1= new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1,book_condition);
        ArrayAdapter<String> adapter2= new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1,course_subject);
        Log.d("EditListing Helper", "Created Adapters");
        //spinners
        spinnerCondition =findViewById(R.id.conditionSpin2);
        spinnerSubject = findViewById(R.id.subjectSpin2);

        spinnerCondition.setAdapter(adapter1);
        spinnerSubject.setAdapter(adapter2);
        Log.d("EditListing Helper", "set adapters");

        //set the saved contents
        db.open();
        Bundle extra = getIntent().getExtras();
        listingID = extra.getString("displayId");
        Log.d("EditListing Helper", listingID);

        final int idList = Integer.parseInt(listingID);

        cur = db.getAll(idList);

        EDIT_TITLE = findViewById(R.id.titleET);
        EDIT_AUTHOR = findViewById(R.id.authorET);
        EDIT_ISBN = findViewById(R.id.isbnET);
        EDIT_PROFNAME = findViewById(R.id.profET);
        EDIT_COURSENUM = findViewById(R.id.courseNumET);
        EDIT_PRICE = findViewById(R.id.priceET);
        EDIT_DESCRIPTION = findViewById(R.id.descriptionET);

        EDIT_TITLE.setText(cur.getString(cur.getColumnIndex("title")));
        EDIT_AUTHOR.setText(cur.getString(cur.getColumnIndex("author")));
        EDIT_PRICE.setText(cur.getString(cur.getColumnIndex("price")));
        EDIT_ISBN.setText(cur.getString(cur.getColumnIndex("isbn")));
        //spinnerSubject.setSelection(cur.getString(cur.getColumnIndex("condition")));
        EDIT_DESCRIPTION.setText(cur.getString(cur.getColumnIndex("description")));
        //subject.setText(cursor.getString(cursor.getColumnIndex("subject")));
        EDIT_COURSENUM.setText(cur.getString(cur.getColumnIndex("course_number")));
        EDIT_PROFNAME.setText(cur.getString(cur.getColumnIndex("professor")));

        btnUpdate = findViewById(R.id.updateBtn);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleEdit = EDIT_TITLE.getText().toString();
                authorEdit = EDIT_AUTHOR.getText().toString();
                isbnEdit = EDIT_ISBN.getText().toString();
                profNameEdit = EDIT_PROFNAME.getText().toString();
                courseNumEdit = EDIT_COURSENUM.getText().toString();
                priceEdit = EDIT_PRICE.getText().toString();
                descriptionEdit = EDIT_DESCRIPTION.getText().toString();
                //get data from spinner
                conditionEdit = spinnerCondition.getSelectedItem().toString();
                subjectEdit = spinnerSubject.getSelectedItem().toString();

                String idToUpdate = Integer.toString(idList);

                db.updateListing(titleEdit, authorEdit, isbnEdit, profNameEdit, courseNumEdit, priceEdit, descriptionEdit,
                        conditionEdit, subjectEdit, idToUpdate);

                Toast.makeText(getBaseContext(), "Listing Updated", Toast.LENGTH_LONG).show();
                Intent i = new Intent(EditListingsActivity.this, ViewListingActivity.class);
                i.putExtra("listId", idToUpdate);
                startActivity(i);

                db.close();

            }

        });


    }
}
