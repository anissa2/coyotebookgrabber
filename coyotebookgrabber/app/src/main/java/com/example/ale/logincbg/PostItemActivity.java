package com.example.ale.logincbg;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by Dell on 12/5/2017.
 */

public class PostItemActivity extends AppCompatActivity {
    //define EditText, Button and Spinner in activity_post_item
    Button Post;
    EditText TITLE, AUTHOR, ISBN, PROFESSOR_NAME, COURSE_NUMBER, PRICE, DESCRIPTION;
    String title, author, isbn, professor_name, course_num, price, description, condition, subject;
    //spinners
    Spinner spinnerCondition, spinnerSubject;

    Context ctx = this;

    private static final int IMAGE_GALLERY_REQUEST = 20;
    private ImageView postImageView;
    Bitmap imageBitMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_item);

        //get a reference to the image view that holds the image that the user will see
        postImageView = (ImageView) findViewById(R.id.postImageView);

        String [] book_condition = new  String[]{"Good",
                "Bad",
                "Acceptable"};
        String [] course_subject= new String[]{
                "ACCT - Accounting",
                "ADMN - Administration",
                "AMST - American Studies",
                "ANTH - Anthropology",
                "ARAB - Arabic",
                "ART - Art",
                "AS - Aerospace Studies",
                "ASIA - Asian Studies",
                "ASL - American Sign Language",
                "ASTR - Astronomy",
                "BIOL - Biology",
                "CAL - Arts and Letters",
                "CD - Child Development",
                "CHEM - Chemistry",
                "CHIN - Chinese",
                "CJUS - Criminal Justice",
                "CM - Course Math",
                "COMM- Communication Studies",
                "CSE - Computer Science and Engineering",
                "CSU - Concurrent Enrollment",
                "DAN - Dance",
                "EADM - Educational Administration",
                "EADV - Advanced Education",
                "ECLG - Educational Counseling",
                "ECON - Economics",
                "ECTS - Career and Technical Studies",
                "EDCA - Correctional and Alternative",
                "ENG - English",
                "EDCI - Curriculum and Instruction",
                "EDSC - Science Education",
                "EDSP - School Psychology",
                "EDUC - Education",
                "EELB - Elementary/Bilingual Education",
                "EENV - Environmental Education",
                "EESL - Educ Eng Speakers other Lang",
                "ENG - English",
                "ENTR - Entrepreneurship",
                "ERDG - Reading Education",
                "EREH - Rehabilitiation Counseling",
                "ES - Ethnic Studies",
                "ESBM - School Business Management",
                "ESEC - Secondary Education",
                "ESPE - Special Education",
                "ESTM - Sci, Tech, Engin, Math Edu",
                "ETEC - Instructional Technology",
                "EVOC - Vocational Education",
                "FIN - Finance",
                "FLAN - Foreign Language",
                "FREN - French",
                "GEOG - Geography",
                "GEOL - Geology",
                "GER - German",
                "GSS - Gender and Sexuality Studies",
                "HD - Human Development",
                "HIST - History",
                "HON - Honors",
                "HRM - Human Resource Management",
                "HSCI - Health Science",
                "HUM - Humanities",
                "IP - International Program",
                "IS- Interdisciplinary Studies",
                "ISA - Interdisciplinary Studies Abro",
                "IST- Information Systems and Tech",
                "JAPN - Japanese",
                "KINE - Kinesiology",
                "KOR - Korean",
                "MAND - Mandarin",
                "MATH - Mathematics",
                "MGMT - Management",
                "MILS - Military Science",
                "MKTG - Marketing",
                "MUS - Music",
                "NSCI - Natural Sciences",
                "NSE - National Student Exchange",
                "NURS - Nursing",
                "OLLI - Lifelong Learning-OSHER",
                "PA - Public Administration",
                "PERS - Persian",
                "PHIL - Philosophy",
                "PHYS - Physics",
                "PLST - Paralegal Studies",
                "PSCI - Political Science",
                "PSYC - Psychology",
                "SCM - Supply Chain Management",
                "SOC - Sociology",
                "SPAN - Spanish",
                "SSCI - Social Sciences",
                "SW - Social Work",
                "TA - Theatre Arts",
                "USTD - University Studies",
        };
        ArrayAdapter<String> adapter= new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1,book_condition);
        ArrayAdapter<String> adapter1= new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1,course_subject);
        Log.d("PostItem Helper", "Created Adapters");
        //spinners
        spinnerCondition =findViewById(R.id.conditionSpin);
        spinnerSubject = findViewById(R.id.subjectSpin);

        spinnerCondition.setAdapter(adapter);
        spinnerSubject.setAdapter(adapter1);
        Log.d("PostItem Helper", "set adapters");

        //assign variables to each EditText
        TITLE = findViewById(R.id.titleET);
        AUTHOR = findViewById(R.id.authorET);
        ISBN = findViewById(R.id.isbnET);
        PROFESSOR_NAME = findViewById(R.id.profET);
        COURSE_NUMBER = findViewById(R.id.courseNumET);
        PRICE = findViewById(R.id.priceET);
        DESCRIPTION = findViewById(R.id.ET_Description);

        Post = (findViewById(R.id.updateBtn));

        Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = TITLE.getText().toString();
                author = AUTHOR.getText().toString();
                isbn = ISBN.getText().toString();
                professor_name = PROFESSOR_NAME.getText().toString();
                course_num = COURSE_NUMBER.getText().toString();
                price = PRICE.getText().toString();
                description = DESCRIPTION.getText().toString();
                //get data from spinner
                condition = spinnerCondition.getSelectedItem().toString();
                subject = spinnerSubject.getSelectedItem().toString();

                if (title.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter a title", Toast.LENGTH_SHORT).show();
                } else if (author.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter an author", Toast.LENGTH_SHORT).show();
                } else if (isbn.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter isbn", Toast.LENGTH_SHORT).show();
                } else if (professor_name.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter professor name", Toast.LENGTH_SHORT).show();
                } else if (course_num.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter course number", Toast.LENGTH_SHORT).show();
                } else if (price.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter a price", Toast.LENGTH_SHORT).show();
                } else if (description.matches("")) {
                    Toast.makeText(getBaseContext(), "You did not enter a description", Toast.LENGTH_SHORT).show();
                } else {
                    ListingDBAdapter DB = new ListingDBAdapter(ctx);
                    String userEmail = userEmail();
                    DB.open();
                    byte [] imagePost = getBytes(imageBitMap);
                    Log.d("Post Item Helper", "bitmap image converted to bytes");
                    DB.postInfo(title, author, isbn, professor_name, course_num, price, description, condition, subject, imagePost, userEmail);
                    Toast.makeText(getBaseContext(), "Textbook Listed!", Toast.LENGTH_LONG).show();
                    finish();
                    DB.close();
                }
            }
        });
    }

    private String userEmail () {
        String email;
        Context ctx = this;
        UserDBAdapter uDB = new UserDBAdapter(ctx);
        uDB.open();
        email = uDB.getUserEmail();
        uDB.close();
        return email;
    }

    public void onImageGalleryClicked (View v) {
        //invoke the image gallery using an implicit intent
        Intent selectPhotoIntent = new Intent(Intent.ACTION_PICK);

        //where do we want to find the data
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        //get uri representation
        Uri data = Uri.parse(pictureDirectoryPath);

        //set the data and type. Get all image types
        selectPhotoIntent.setDataAndType(data, "image/*");

        //invoke this activity and get something back
        startActivityForResult(selectPhotoIntent, IMAGE_GALLERY_REQUEST);
    }

    //receive startActivityForResult RESULT in this method
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //requestCode = IMAGE_GALLERY_REQUEST constant, need to know what activity needs to send data
        //resultCode = will tell if activity executed ok or user exited out of it
        //data = any data that result returns which we will use
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            //if we are here everything processed successfully
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                //if we are here we are hearing back from image gallery

                //will return a Uri. Uniform resource identifier = the address of the image on the sd card
                Uri imageUri = data.getData();
                Log.d("PostItem Helper", "Data grabbed");
                Log.d("Data grabbed", imageUri.toString());
                //declare a stream to read the image data from the SD card
                InputStream inputStream;

                //try/catch to get any data that was failed during read

                //we are getting an input stream based on the uri of an image
                try {
                    inputStream = getContentResolver().openInputStream(imageUri);
                    //get a bitmap from the stream
                    imageBitMap = BitmapFactory.decodeStream(inputStream);
                    Log.d("PostItem Helper", "BitMap Created");
                    //show the image to the user
                    postImageView.setImageBitmap(imageBitMap);
                    Log.d("PostItem Helper", "image posted in view");

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //show message to user indicating that image is unavailable
                    Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
}
