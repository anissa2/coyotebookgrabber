package com.example.ale.logincbg;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by Dell on 3/12/2018.
 */

public class DisplayListingActivity extends Activity {
    ListingDBAdapter db = new ListingDBAdapter(this);
    Cursor cr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_post_activity);
        displayPosts();
    }

    public void displayPosts() {
        db.open();
        String email = userEmail();
        cr = db.viewPosts(email);

        String fromFieldNames[] = new String[]{db.POSTS_TEXT_TITLE, db.POSTS_TEXT_AUTHOR, db.POSTS_TEXT_PRICE};
        int[] toViewListings = new int[]{R.id.listTitleTxt, R.id.listAuthorTxt, R.id.listPriceTxt};

        final CustomAdapter2 sc2 = new CustomAdapter2(getBaseContext(), R.layout.list_layout2, cr, fromFieldNames, toViewListings);

        ListView list2 = findViewById(android.R.id.list);
        list2.setAdapter(sc2);
        db.close();
    }

    private String userEmail () {
        String email;
        Context ctx = this;
        UserDBAdapter uDB = new UserDBAdapter(ctx);
        uDB.open();
        email = uDB.getUserEmail();
        uDB.close();
        return email;
    }
}
