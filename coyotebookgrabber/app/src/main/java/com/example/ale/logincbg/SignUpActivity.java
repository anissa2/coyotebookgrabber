package com.example.ale.logincbg;

import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.content.Context;
import android.view.View.OnClickListener;
import android.widget.Toast;

/**
 * This file registers a new user. It checks for valid password and valid email
 */

public class SignUpActivity extends AppCompatActivity {
    EditText USER_FNAME, USER_LNAME, USER_USERNAME, USER_EMAIL, USER_PASS, CON_PASS;
    String user_fname, user_lname, user_username, user_email, user_pass, con_pass, passEcnryp;
    Button CREATE_ACCOUNT;
    Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final String logged_user = UserDBAdapter.USER_IS_CURRENT_USER;
        USER_FNAME = findViewById(R.id.ET_fname);
        USER_LNAME = findViewById(R.id.ET_lname);
        USER_USERNAME = findViewById(R.id.ET_username);
        USER_EMAIL = findViewById(R.id.ET_Email);
        USER_PASS = findViewById(R.id.ET_password);
        CON_PASS = findViewById(R.id.ET_pass_con);
        CREATE_ACCOUNT = (findViewById(R.id.bSignUp));


        CREATE_ACCOUNT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                user_fname = USER_FNAME.getText().toString();
                user_lname = USER_LNAME.getText().toString();
                user_username = USER_USERNAME.getText().toString();
                user_email = USER_EMAIL.getText().toString();
                user_pass = USER_PASS.getText().toString();
                con_pass = CON_PASS.getText().toString();

                String email_pattern = "[0-9]+@[coyote]+\\.+[csusb]+\\.+[edu]+";

                //check data integrity
                if (!(user_email.matches(email_pattern))) {
                    Toast.makeText(getBaseContext(), "Please use your coyote email", Toast.LENGTH_LONG).show();
                    USER_EMAIL.setText("");
                } else if (!(user_pass.equals(con_pass))) {
                    Toast.makeText(getBaseContext(), "Passwords do not match", Toast.LENGTH_LONG).show();
                    USER_PASS.setText("");
                    CON_PASS.setText("");
                } else {
                    //encrypt password
                    try {
                        passEcnryp = AESCrypt.encrypt(user_pass);
                        Log.d("Password Encrypted", passEcnryp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //save in database
                    UserDBAdapter DB = new UserDBAdapter(ctx);
                    DB.open();
                    DB.createUser(user_fname, user_lname, user_username, user_email, passEcnryp, logged_user);
                    Toast.makeText(getBaseContext(), "Registration Successful", Toast.LENGTH_LONG).show();
                    finish();
                    DB.close();
                }
            }
        });
    }
}

