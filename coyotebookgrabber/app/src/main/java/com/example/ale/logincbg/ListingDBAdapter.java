package com.example.ale.logincbg;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.SQLException;
import android.content.ContentValues;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Dell on 1/3/2018.
 */

public class ListingDBAdapter {

    public static final String ID = "_id";
    public static final String POSTS_TEXT_TITLE = "title";
    public static final String POSTS_TEXT_AUTHOR = "author";
    public static final String POSTS_TEXT_ISBN = "isbn";
    public static final String POSTS_PROFESSOR_NAME = "professor";
    public static final String POSTS_COURSE_NUMBER = "course_number";
    public static final String POSTS_TEXT_PRICE = "price";
    public static final String POSTS_TEXT_DESCRIPTION = "description";
    public static final String POSTS_TEXT_CONDITION  = "condition";
    public static final String POSTS_COURSE_SUBJECT = "subject";
    public static final String POSTS_IMAGE = "image_data";
    public static final String POSTS_USER_EMAIL = UserDBAdapter.USER_COYOTE_EMAIL;

    protected static final String DATABASE_TABLE_POSTS = "table_posts";

    private DatabaseHelper listingDBHelper;
    private SQLiteDatabase listingDB;

    private final Context listingCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public ListingDBAdapter(Context ctx) {

        this.listingCtx = ctx;
        Log.d("ListingDBAdapter Helper", "Posts open/close database");
    }

    public ListingDBAdapter open() throws SQLException {
        this.listingDBHelper = new DatabaseHelper(this.listingCtx);
        this.listingDB = this.listingDBHelper.getWritableDatabase();
        Log.d("ListingDBAdapter Helper","Database opened");
        return this;
    }

    public void close() {
        this.listingDBHelper.close();
    }

    public long postInfo(String title, String author, String isbn,
                         String professor, String course_num, String price, String description,
                         String condition, String subject, byte[] image, String user_email) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(POSTS_TEXT_TITLE, title);
        initialValues.put(POSTS_TEXT_AUTHOR, author);
        initialValues.put(POSTS_TEXT_ISBN, isbn);
        initialValues.put(POSTS_PROFESSOR_NAME, professor);
        initialValues.put(POSTS_COURSE_NUMBER, course_num);
        initialValues.put(POSTS_TEXT_PRICE, price);
        initialValues.put(POSTS_TEXT_DESCRIPTION, description);
        initialValues.put(POSTS_TEXT_CONDITION , condition);
        initialValues.put(POSTS_COURSE_SUBJECT, subject);
        initialValues.put(POSTS_IMAGE, image);
        initialValues.put(POSTS_USER_EMAIL, user_email);
        Log.d("ListingDBAdapter Helper", "Post Listed");
        return this.listingDB.insert(DATABASE_TABLE_POSTS, null, initialValues);
    }

    public boolean deletePost(long rowId) {

        return this.listingDB.delete(DATABASE_TABLE_POSTS, ID + "=" + rowId, null) > 0; //$NON-NLS-1$
    }

    public Cursor searchListing(String query) {
        Cursor c;
        c =  this.listingDB.rawQuery("SELECT _id, title, price, author FROM " + DATABASE_TABLE_POSTS + " WHERE title = '" + query + "'", null);
        if (c != null) {
            if(c.moveToFirst()) {
                do {
                    String title = c.getString(c.getColumnIndex("title"));
                    //results.add(title);
                    Log.d("Listing DB search query title: ", title);

                    String price = c.getString(c.getColumnIndex("price"));
                    //results.add(price);
                    Log.d("Listing DB search query price: ", price);

                    String author = c.getString(c.getColumnIndex("author"));
                    //results.add(author);
                    Log.d("Listing DB search query author: ", author);

                } while (c.moveToNext());
            }
        }
        Log.d("ListDBHelper Search Processed", "One row selected");
        return c;
    }

    public Cursor getAll(int id) {
        Cursor c;
        c =  this.listingDB.rawQuery("SELECT * FROM " + DATABASE_TABLE_POSTS + " WHERE _id = '" + id + "'", null);
        if (c != null) {
            c.moveToFirst();
        }
        Log.d("ListDBHelper Get All Processed", "One row selected");
        return c;
    }

    public Cursor viewPosts(String email) {
        Cursor cr;
        cr =  this.listingDB.rawQuery("SELECT _id, title, price, author FROM " + DATABASE_TABLE_POSTS + " WHERE coyote_email = '" + email + "'", null);
        if (cr != null) {
            if(cr.moveToFirst()) {
                do {
                    String title = cr.getString(cr.getColumnIndex("title"));
                    Log.d("Listing DB search query title: ", title);

                    String price = cr.getString(cr.getColumnIndex("price"));
                    Log.d("Listing DB search query price: ", price);

                    String author = cr.getString(cr.getColumnIndex("author"));
                    Log.d("Listing DB search query author: ", author);

                } while (cr.moveToNext());
            }
        }
        Log.d("ListDBHelper", "Listings Retrieved");
        return cr;
    }

    public long updateListing(String title, String author, String isbn,
                         String professor, String course_num, String price, String description,
                         String condition, String subject, String id) {
        ContentValues cv = new ContentValues();
        cv.put(POSTS_TEXT_TITLE, title);
        cv.put(POSTS_TEXT_AUTHOR, author);
        cv.put(POSTS_TEXT_ISBN, isbn);
        cv.put(POSTS_PROFESSOR_NAME, professor);
        cv.put(POSTS_COURSE_NUMBER, course_num);
        cv.put(POSTS_TEXT_PRICE, price);
        cv.put(POSTS_TEXT_DESCRIPTION, description);
        cv.put(POSTS_TEXT_CONDITION , condition);
        cv.put(POSTS_COURSE_SUBJECT, subject);
        Log.d("ListingDBAdapter Helper", "Listing Updated");

        return this.listingDB.update(DATABASE_TABLE_POSTS, cv, "_id = ?", new String[]{id});
    }

}



