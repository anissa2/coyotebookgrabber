package com.example.ale.logincbg;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.SQLException;
import android.content.ContentValues;
import android.util.Log;

/**
 * Created by Dell on 1/3/2018.
 */

public class UserDBAdapter {

    public static final String ID = "_id";
    public static final String USER_FNAME = "fname";
    public static final String USER_LNAME = "lname";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASS = "password";
    public static final String USER_COYOTE_EMAIL = "coyote_email";
    public static final String USER_IS_CURRENT_USER = "current_user";

    protected static final String DATABASE_TABLE_USERS = "table_users";

    private DatabaseHelper userDBHelper;
    private SQLiteDatabase userDB;

    private final Context userCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx
     *            the Context within which to work
     */
    public UserDBAdapter(Context ctx) {

        this.userCtx = ctx;
        Log.d("UserDBadapter Helper", "Context to open/close database");
    }

    /**
     * Open the users database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException
     *             if the database could be neither opened or created
     */
    public UserDBAdapter open() throws SQLException {
        this.userDBHelper = new DatabaseHelper(this.userCtx);
        this.userDB = this.userDBHelper.getWritableDatabase();
        Log.d("UserDBAdapter Helper","Database opened");
        return this;
    }

    /**
     * close return type: void
     */
    public void close() {
        this.userDBHelper.close();
    }

    /**
     * Create a new user. If the user is successfully created return the new
     * Id for that user, otherwise return a -1 to indicate failure.
     *
     * @param fname
     * @param lname
     * @param username
     * @param password
     * @param coyote_email
     * @param isCurrentUser
     * @return rowId or -1 if failed
     */
    public long createUser(String fname, String lname, String username,
                           String coyote_email, String password, String isCurrentUser){
        ContentValues initialValues = new ContentValues();
        isCurrentUser = "FALSE";
        initialValues.put(USER_FNAME, fname);
        initialValues.put(USER_LNAME, lname);
        initialValues.put(USER_USERNAME, username);
        initialValues.put(USER_COYOTE_EMAIL, coyote_email);
        initialValues.put(USER_PASS, password);
        initialValues.put(USER_IS_CURRENT_USER, isCurrentUser);
        Log.d("UserDBAdapter Helper", "Student Account created");
        return this.userDB.insert(DATABASE_TABLE_USERS, null, initialValues);
    }

    /**
     * Delete the user with the given ID
     *
     * @param rowId
     * @return true if deleted, false otherwise
     */
    public boolean deleteUser(long rowId) {

        return this.userDB.delete(DATABASE_TABLE_USERS, ID + "=" + rowId, null) > 0; //$NON-NLS-1$
    }

    //grabs login info from database
    public Cursor getLoginInfo(String username) {
        Cursor CR = this.userDB.rawQuery("SELECT username, password FROM " + DATABASE_TABLE_USERS + " WHERE username = '" + username + "'", null );
        if (CR != null) {
            CR.moveToFirst();
        }
        Log.d("UserDBAdapter Helper", "One row selected");
        return CR;
    }

    public void setLoggedUser (String username) {
        String user_logged = "True";
        ContentValues values = new ContentValues();
        values.put(USER_IS_CURRENT_USER, user_logged);
        this.userDB.update(DATABASE_TABLE_USERS, values, " username " + " = " + "'" + username + "'", null);
        Log.d("UserDBAdapter Helper", "One row updated");
    }

    public void unsetLoggedUser () {
        String user_not_logged = "False";
        ContentValues values = new ContentValues();
        values.put(USER_IS_CURRENT_USER, user_not_logged);
        this.userDB.update(DATABASE_TABLE_USERS, values,  " current_user = 'True'", null);
        Log.d("UserDBAdapter", "user unset");
    }

    public String getUserEmail() {
        String email = "";
        //SQLiteDatabase SQ = db.getReadableDatabase();
        Cursor cr = this.userDB.rawQuery("SELECT coyote_email FROM " + DATABASE_TABLE_USERS + " WHERE current_user = 'True'", null);
        if (cr.moveToFirst()) {
            email = cr.getString(cr.getColumnIndex("coyote_email"));
        }
        cr.close();
        Log.d("UserDBAdapter", "fetched email");
        return email;
    }

    public String getFname() {
        String fname = "";
        Cursor cr = this.userDB.rawQuery("SELECT fname FROM " + DATABASE_TABLE_USERS + " WHERE current_user = 'True'", null);
        if (cr.moveToFirst()) {
            fname = cr.getString(cr.getColumnIndex("fname"));
        }
        cr.close();
        Log.d("UserDBAdapter", "First Name fetched");
        return fname;
    }

    public String getLname() {
        String lname = "";
        Cursor cr = this.userDB.rawQuery("SELECT lname FROM " + DATABASE_TABLE_USERS + " WHERE current_user = 'True'", null);
        if (cr.moveToFirst()) {
            lname = cr.getString(cr.getColumnIndex("lname"));
        }
        cr.close();
        Log.d("UserDBAdapter", "Last Name fetched");
        return lname;
    }

}
